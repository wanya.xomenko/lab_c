#include<stdio.h>

int main()
{
	int current_february = 28;
	int january = 31;
	int february = 29;
	int april = 30;
	int march = 31;
	int may = 31;
	int july = 31;
	int august = 31;
	int october = 31;
	int december = 31;
	int june = 30;
	int september = 30;
	int november = 30;
	int quart_1_current = january + current_february + march;
	int quart_1 = january + february + march;
	int quart_2 = april + may + june;
	int quart_3 = july + august + september;
	int quart_4 = october + november + december;
	
	printf("Days in quarters (2019)\n");
	printf("Days in Q1 of the current year: %d\n", quart_1_current);
	printf("Days in Q2 of the current year: %d\n", quart_2);
	printf("Days in Q3 of the current year: %d\n", quart_3);
	printf("Days in Q4 of the current year: %d\n\n", quart_4);
	
	printf("Days in quarters of the year\n");
	printf("Days in Q1 of the year: %d\n", quart_1);
	printf("Days in Q2 of the year: %d\n", quart_2);
	printf("Days in Q3 of the year: %d\n", quart_3);
	printf("Days in Q4 of the year: %d\n", quart_4);
	
	return 0;
}