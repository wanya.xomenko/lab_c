#include <stdio.h>
int main()
{
int daysCurrentFebruary = 29;
int daysJanuary = 31;
int daysFebruary = daysCurrentFebruary;
int daysMarch = 31;
int daysApril = 30;
int daysMay = 31;
int daysJune = 30;
int daysJuly = 31;
int daysAugust = 31;
int daysSeptember = 30;
int daysOctober = 31;
int daysNovember = 30;
int daysDecember = 31;
int daysFirstHalf = daysJanuary + daysFebruary + daysMarch
+ daysApril + daysMay + daysJune;
int daysSecondHalf = daysJuly + daysAugust + daysSeptember
+ daysOctober + daysNovember + daysDecember;
printf("Days in the first half of the current year: %d\n", daysFirstHalf);
printf("Days in the second half of the current year: %d\n", daysSecondHalf);
printf("Days in the current year: %d\n", daysFirstHalf + daysSecondHalf);
return 0;
}

